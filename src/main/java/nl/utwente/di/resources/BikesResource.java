package nl.utwente.di.resources;

import nl.utwente.di.dao.BikeDao;
import nl.utwente.di.model.Bike;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/bikes")
public class BikesResource {
    @POST
    @Path("/bikes")
    public void createBike(Bike bike) {
        BikeDao.instance.getBikeCollection().put(bike.getID(), bike);
    }
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Bike> getBikes() {
        return new ArrayList<>(BikeDao.instance.getBikeCollection().values());
    }

    @GET
    @Path("/bikes/{bikeid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Bike getBikeDetails(@PathParam("bikeid") String id) {
        return null;
    }
    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Bike updateBike(Bike bike) {
        return bike;
    }
    @DELETE
    @Path("/bikes/{bikeid}")
    public void deleteBike(@PathParam("bikeid") String id) {

    }
    @POST
    @Path("/bikes/{bikeid}/order")
    public boolean orderBike() {
        return false;
    }
}
