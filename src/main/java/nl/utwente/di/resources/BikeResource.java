package nl.utwente.di.resources;

import nl.utwente.di.dao.BikeDao;
import nl.utwente.di.model.Bike;

public class BikeResource extends BikesResource {
    @Override
    public Bike getBikeDetails(String id) {
        return BikeDao.instance.getBikeCollection().get(id);
    }
    @Override
    public Bike updateBike(Bike bike) {
        return bike;
    }
    @Override
    public void deleteBike(String id) {
        BikeDao.instance.getBikeCollection().remove(id);
    }
    @Override
    public boolean orderBike() {
        return false;
    }
}
