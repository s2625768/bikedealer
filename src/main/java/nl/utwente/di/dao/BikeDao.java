package nl.utwente.di.dao;

import nl.utwente.di.model.Bike;

import java.util.HashMap;
import java.util.Map;

public enum BikeDao {
    instance;

    private Map<String, Bike> bikeCollection = new HashMap<>();

    public Map<String, Bike> getBikeCollection() {
        return bikeCollection;
    }
}
