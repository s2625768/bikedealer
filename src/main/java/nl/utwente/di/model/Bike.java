package nl.utwente.di.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Bike {
    String ID;
    String ownerName;
    String colour;
    String gender;

    public Bike(String ID, String ownerName, String colour, String gender) {
        this.ID = ID;
        this.ownerName = ownerName;
        this.colour = colour;
        this.gender = gender;
    }

    public String getID() {
        return ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }
    public String getOwnerName() {
        return ownerName;
    }
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    public String getColour() {
        return colour;
    }
    public void setColour(String colour) {
        this.colour = colour;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
}
