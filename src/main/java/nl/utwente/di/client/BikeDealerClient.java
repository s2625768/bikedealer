package nl.utwente.di.client;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import nl.utwente.di.model.Bike;

public class BikeDealerClient {

    public static void main(String[] args) {

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(getBaseURI());
        String url_pattern = "rest";
        String path = "bikes";

        // ===
        // create bike 1
        Bike bike = new Bike("1", "owner1", "red", "female");
        Response response = target.path(url_pattern).path(path)
                .request(MediaType.APPLICATION_XML)
                .post(Entity.entity(bike, MediaType.APPLICATION_XML));
        // Return code for created resource
        System.out.println("> create bike - result status: "
                + response.getStatus());
        // create bike 2
        bike = new Bike("2", "owner2", "blue", "female");
        response = target.path(url_pattern).path(path)
                .request(MediaType.APPLICATION_XML)
                .post(Entity.entity(bike, MediaType.APPLICATION_XML));

        // ===
        // Get all Bikes
        // in JSON
        System.out.println("> all bikes (json): "
                + target.path(url_pattern).path(path).request()
                .accept(MediaType.APPLICATION_JSON).get(String.class));
        // in XML
        System.out.println("> all bikes (xml): "
                + target.path(url_pattern).path(path).request()
                .accept(MediaType.APPLICATION_XML).get(String.class));

        // ===
        // Get all "blue" Bikes
        // in XML
        System.out.println("> blue bikes: "
                + target.path(url_pattern).path(path)
                .queryParam("color", "blue").request()
                .accept(MediaType.APPLICATION_XML).get(String.class));

        // ===
        // Get the Bike with id 1
        System.out.println("> get bike with id = 1: "
                + target.path(url_pattern).path(path + "/1").request()
                .accept(MediaType.APPLICATION_XML).get(String.class));

        // ===
        // Delete the Bike with id 1
        target.path(url_pattern).path(path + "/1").request().delete();
        // Get the all bikes, bike with id 1 should not be present
        System.out.println("> get all bikes after delete bike 1: "
                + target.path(url_pattern).path(path).request()
                .accept(MediaType.APPLICATION_XML).get(String.class));
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080/bikeDealer").build();
    }
}